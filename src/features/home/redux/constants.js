export const CORE_API = process.env.CORE_API;
export const SOURCE_API = process.env.SOURCE_API;
export const ADMINTOKEN = process.env.ADMINTOKEN;
export const VERSION = process.env.VERSION;
