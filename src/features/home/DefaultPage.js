import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  Card, Button, CardImg, CardTitle, CardSubtitle, CardBody, Input
} from 'reactstrap';import * as actions from './redux/actions';
import logo from '../../images/logo.png';
import { Lightbox } from "react-modal-image";
import axios from 'axios';
import Select from 'react-select';
import Swal from 'sweetalert2';
import 'sweetalert2/src/sweetalert2.scss' ;
import {CORE_API, SOURCE_API, ADMINTOKEN, VERSION} from './redux/constants';


export class DefaultPage extends Component {
  static propTypes = {
    home: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };
  constructor(props) {
		super(props);

		this.state = {
      source_results: null,
      source_files: null,
      pageNumber: null,
      newName: null,
      sources: [],
      rateCards: [],
      ratecardId: null,
      savedData: [],
      indexArray: [],
      buttonState: 'Comfirm',
      sourceId: null,
      source: null,
      isValid: false,
      showImage: null,
		}
	}
  componentWillMount= ()=> {
    this.get_sources();
  }

  get_source_id = (sourceName) => {
    const query = {
        url: CORE_API,
        method: 'Post',
        headers: {
          Authorization: ADMINTOKEN,
        },
        data: {
          query: `query($filterByName: String){
              sources(filterByName:$filterByName){
                  id
              }
          }`,
          variables: {
            filterByName: sourceName,
          },
        },
      };

      axios(query).then((res) => {
        let temp_sourceId = null
        if (res.data.data.sources.length !== 0)
        {
          temp_sourceId = res.data.data.sources[0].id
          this.sourceHandleChange(res.data.data.sources[0].id);
        }
        this.setState({ sourceId: temp_sourceId});
        this.forceUpdate();
    }).catch((e)=>{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: `Something went wrong!..${e.message}`,
        footer: '<a href>contact osl support</a>'
      });
    });
  }

  save = (props, index) => {
    if (this.state.newName !== null) {
      if(props.name.split('.')[1] !== this.state.newName.split('.')[1]) {
          Swal.fire({
          icon: 'error',
          title: 'File Extension Error',
          text: `Correct extension = ${props.name.split('.')[1]}, please fixed renamed file name extension.`,
          footer: '<a href>contact osl support</a>'
        });
      }
      else {
      this.state.indexArray.push(index);
      this.state.savedData.push({ "sourceId":this.state.sourceId,"filename": props.name,"new_name":this.state.newName,"page":this.state.pageNumber,"rate_card": this.state.ratecardId });
      this.forceUpdate();
      }
    } else {
      this.state.indexArray.push(index);
      this.state.savedData.push({ "sourceId":this.state.sourceId,"filename": props.name,"new_name":this.state.newName,"page":this.state.pageNumber,"rate_card": this.state.ratecardId });
      this.forceUpdate();
    }
  }
  renaming = (original, renamed) => {
    console.log(original);
    this.setState({ isValid:true });
    this.forceUpdate();
  }
  viewImage =(imag)=> {

    this.setState({ showImage : imag });
    this.forceUpdate();
  }
  closeLightbox = () => {
    this.setState({ showImage : null });
    this.forceUpdate();
  };
  processAll = async() => {
    await axios.post(`${SOURCE_API}/processPrint`,{"source_files": this.state.savedData, "source_name": this.state.source}).then((res)=>{
      window.location.reload();
    }).catch((e)=>{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: `Something went wrong!.. ${e.message}</b>`,
        footer: '<a href>Please contact OSL Support</a>'
      });
    });
  }
  get_sources = async () => {
    await axios.post(`${SOURCE_API}/sourcedirectory`).then((res)=>{
      this.setState({
        source_results: res.data.sources
      })
    }).catch((e)=>{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: `Something went wrong!..${e.message}`,
        footer: '<a href>Please contact OSL Support</a>'
      });
    });
    this.forceUpdate();
  }
  handleChange = selectedOption => {
    this.setState({ selectedOption, savedData: [],indexArray: [] });
    this.getFiles(selectedOption.label);
    this.setState({ source : selectedOption.label });
    this.forceUpdate();
    this.get_source_id(selectedOption.label.split("_")[0])
  };

  sourceHandleChange = selectedSource => {
    this.forceUpdate();
    const query = {
        url: CORE_API,
        method: 'Post',
        headers: {
          Authorization: '',
        },
        data: {
          query: ` query($sourceId: UUID) {
            rateCards(sourceId: $sourceId){
              id
              programName
              rate
            }
          }`,
          variables: {
            sourceId: selectedSource,
          },
        },
      };

     axios(query).then((res) => {
       console.log(res);
       if(res.data.data.rateCards !== undefined )
       {
        res.data.data.rateCards.map((item) =>
          this.state.rateCards.push({"label":item.programName + " - " + item.rate, "value":item.id })
        )
       }
    }).catch((e)=>{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: `Something went wrong!..${e.message}`,
        footer: '<a href>Please contact OSL Support</a>'
      });
    });
  }
  rateCardHandleChange =async selectedRateCard => {
    this.setState({ ratecardId : selectedRateCard.value })
  }

  getFiles = async (selectedOption) => {
     await axios.post(`${SOURCE_API}/sourcefiles`,{"directory": selectedOption} ).then((res)=>{
       console.log(res);
      this.setState({
        source_files: res.data.files
      })
    }).catch((e)=>{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: `Something went wrong!.. ${e.message}</b>`,
        footer: '<a href>Please contact OSL Support</a>'
      });
    });
  }

  render() {
    const { selectedOption, selectedRateCard } = this.state;
    return (
      <div className="home-default-page">
        <header className="app-header">
          <img src={logo} className="applogo" alt="logo" />
          <p className="version">version: {VERSION}</p>
        </header>
        <div className="app-intro">
        {this.state.showImage !== null ? 
          <Lightbox
            large={this.state.showImage}
            small={this.state.showImage}
            medium={this.state.showImage}
            onClose={this.closeLightbox}
            showRotate={true}
          />
        :null}
        <br/>
          <h3><b>Source Name:</b> <p style={{color: 'red' }}> {selectedOption!=null ? selectedOption.label.split("_")[0]: null} </p></h3><br />
          <h3><b>Total Confirmed:</b><p style={{color: 'red' }}> ({this.state.savedData.length !== 0 ? this.state.savedData.length :0 }) of ({this.state.source_files !== null ? this.state.source_files.length :0 }) </p></h3>
          <div className="selectDrop">
            Select Source : <Select
              value=''
              onChange={this.handleChange}
              options={this.state.source_results !==null ? this.state.source_results: []}
            />
          </div>
           {this.state.source_files !== null ?
           <div>
            <Button color="warning" className="unLock" style={{ marginTop: "-74px"}} onClick={() => this.processAll()}> process all </Button>
            <div class="row">
              {this.state.source_files.map((x, index) => 
                <div class="col-md-4">
                  <Card key={index}>
                    <CardImg top width="100%" src={x.filepath} alt="Card image cap" onClick={() => this.viewImage(x.filepath) } />
                    <CardBody>
                      <CardTitle><b>Original File Name:</b><Input type="text" id="filename" placeholder="...new file name..." value={x.name} readOnly/></CardTitle>
                      <CardSubtitle><b>Rename File:</b><Input type="text" id="filename" placeholder="...eg. source_yyyymmdd_page.jpg..." onChange={(e) => this.setState({newName: e.target.value })}/></CardSubtitle>
                      <br /><b>Rate Card:</b> <Select
                        value={selectedRateCard}
                        onChange={this.rateCardHandleChange}
                        options={this.state.rateCards}
                      />
                      <br /><b>Page Number:</b><Input type="text" id="page" placeholder="...page number..." onChange={(e) => this.setState({pageNumber: e.target.value })}/><br/>
                      <Button color="primary" className="unLock"  onClick={() => this.save(x,index)}> Confirm </Button> {' '}
                      {this.state.indexArray.includes(index) === true ? <Button color="success" className="unLock" > Confirmed - {x.name}</Button>: null}
                    </CardBody>
                  </Card>
                </div>
                )}
              </div>
            </div>
          : null}
        </div>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    home: state.home,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultPage);
